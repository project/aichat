<?php

namespace Drupal\Tests\aichat\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the installation and uninstallation processes of the aichat module.
 */
class AichatModuleLifecycleTest extends BrowserTestBase {
  
  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['aichat'];

  /**
   * Tests aichat module installation.
   */
  public function testModuleInstallation() {
    $this->assertTrue(\Drupal::moduleHandler()->moduleExists('aichat'), 'The aichat module is installed.');
  }

  /**
   * Tests aichat module uninstallation.
   */
  public function testModuleUninstallation() {
    \Drupal::service('module_installer')->uninstall(['aichat']);
    $this->assertFalse(\Drupal::moduleHandler()->moduleExists('aichat'), 'The aichat module is uninstalled.');
  }

}
