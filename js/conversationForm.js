(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.aichat_conversationForm = {
    attach: function (context, settings) {

      if ($('#aichat-conversation-form .aichat-jsloaded').length) {
        return;
      }
      $('#aichat-conversation-form form').addClass('aichat-jsloaded');

      // remove textarea focus on submit (for mobile)
      let $textarea = $("form.aichat-conversation-form textarea");

      // Before form submit
      var ajaxBeforeSubmitOriginal = Drupal.Ajax.prototype.beforeSubmit;
      Drupal.Ajax.prototype.beforeSubmit = function (form_values, element, options) {
        $textarea.blur();
        ajaxBeforeSubmitOriginal.call(this, form_values, element, options);
      }

      // After form submit
      $.fn.aichatFormAjaxSuccess = function(argument) {

        // Focus on last result (for mobile)
        if (window.innerWidth < 992) {
          $("form.chat-role-user article:last")[0].scrollIntoView();
        }
      }
    }
  }

})(jQuery, Drupal);
