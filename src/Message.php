<?php

namespace Drupal\aichat;

/**
 * Defines the AI chat message for default backend.
 *
 * @see \Drupal\aichat\Plugin\AIChatBackend\DefaultBackend
 */
class Message extends MessageBase {

  /**
   * {@inheritdoc}
   */
  public function setResponseData(array $data):void {
    parent::setResponseData($data);

    // fill this message object with data got from API response
    if (empty($this->getId())) {
      $this->setId( $data['id'] );
    }
    $this->setCreated( $data['created'] );
    $this->setText( $data['choices'][0]['message']['content'] );
    $this->setRole( $data['choices'][0]['message']['role'] );
    $this->removeFlag('error');
  }
}
