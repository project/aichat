<?php

namespace Drupal\aichat;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining aichat entities.
 *
 * @ingroup aichat
 */
interface AIChatInterface extends ContentEntityInterface {

  /**
   * Gets a value from nested conversation data.
   * 
   * @param mixed $key
   *   If $key is a string, it will return configuration value by this key.
   *   If $key is an array, each element of the array will be used as a nested key starting with the outermost key.
   *   For example, $fieldname = ['foo', 'bar'] will return $configuration['foo']['bar'].
   * 
   * @return mixed
   *   The data value for the requested key. NULL if the key does not exist.
   */
  public function getDataNestedValue(mixed $key): mixed;

  /**
   * Sets a value into nested conversation data.
   * 
   * @param mixed $key
   *   An array of parent keys, starting with the outermost key. Or single key string.
   * @param mixed $value
   *   The data value to set. If NULL is provided, key will be unset.
   * @param bool $save
   *   If should run saveData() method
   */
  public function setDataNestedValue(mixed $key, mixed $value, bool $save): void;

  /**
   * Set data field to entity.
   */
  public function saveData(): void;

}
