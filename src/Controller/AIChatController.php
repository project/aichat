<?php

namespace Drupal\aichat\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;
use Drupal\aichat\AIChatTypeInterface;
use Drupal\aichat\AIChatInterface;

class AIChatController extends ControllerBase {

  /**
   * Displays add links for available types.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request object.
   *
   * @return array
   *   A render array for a list of the entity types that can be added.
   */
  public function add(Request $request) {
    $build = [
      '#theme' => 'entity_add_list',
      '#cache' => [
        'tags' => $this->entityTypeManager()->getDefinition('aichat_type')->getListCacheTags(),
      ],
    ];

    $bundles = [];

    $types = $this->entityTypeManager()->getStorage('aichat_type')->loadMultiple();

    foreach ($types as $type) {
      $bundles[$type->id()] = [
        'add_link' =>  Link::fromTextAndUrl($type->label(), Url::fromRoute('aichat.aichat_add_form', ['aichat_type' => $type->id()]))->toString(),
        'label' => $type->label(),
        'description' => $type->getDescription(),
      ];
    }

    $build['#bundles'] = $bundles;
    return $build;
  }

  /**
   * Presents the entity creation form.
   *
   * @param \Drupal\aichat\AIChatTypeInterface $aichat_type
   *   The aichat type to add.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request object.
   *
   * @return array
   *   A form array as expected by
   *   \Drupal\Core\Render\RendererInterface::render().
   */
  public function addForm(AIChatTypeInterface $aichat_type, Request $request) {
    $entity = $this->entityTypeManager()->getStorage("aichat")->create([
      'type' => $aichat_type->id(),
    ]);
    return $this->entityFormBuilder()->getForm($entity);
  }

  /**
   * Provides the page title for this controller.
   *
   * @param \Drupal\aichat\AIChatTypeInterface $aichat_type
   *   The aichat type being added.
   *
   * @return string
   *   The page title.
   */
  public function getAddFormTitle(AIChatTypeInterface $aichat_type) {
    return $this->t('Add %type aichat', ['%type' => $aichat_type->label()]);
  }

  /**
   * Provides the page title for this controller.
   *
   * @param \Drupal\aichat\AIChatInterface $aichat
   *   The aichat being viewed.
   *
   * @return string
   *   The page title.
   */
  public function getAIChatTitle(AIChatInterface $aichat) {
    return $aichat->label();
  }
}
