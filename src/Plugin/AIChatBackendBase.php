<?php

namespace Drupal\aichat\Plugin;

use Drupal\aichat\MessageBase as Message;
use Drupal\aichat\MessageInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\Error;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for AI Chat Backend plugins.
 */
abstract class AIChatBackendBase extends PluginBase implements AIChatBackendInterface {

  use StringTranslationTrait;

  protected $aichat_type;
  protected $aichat;
  protected $messages;

  /**
   * {@inheritdoc}
   */
  public function createNewMessageObject(): MessageInterface {
    return new Message($this);
  }

  /**
   * {@inheritdoc}
   */
  abstract public function defineBackendConfig(): array;

  /**
   * {@inheritdoc}
   */
  abstract public function buildBackendConfigForm(array $form, FormStateInterface $form_state): array;

  /**
   * {@inheritdoc}
   */
  abstract public function validateBackendConfigForm(array $form, FormStateInterface $form_state): void;

  /**
   * {@inheritdoc}
   */
  abstract public function loadMessages(): void;

  /**
   * {@inheritdoc}
   */
  abstract public function saveMessage(MessageInterface $message): void;

  /**
   * {@inheritdoc}
   */
  abstract public function sendMessage(MessageInterface $message): array;

  /**
   * {@inheritdoc}
   */
  abstract public function repeatMessage(MessageInterface $response_message): array;

  /**
   * {@inheritdoc}
   */
  public function getPluginId(): string {
    return $this->pluginId;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginLabel(): string {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getMessages(bool $reload = FALSE): array {
    if (empty($this->messages) || $reload) {
      $this->loadMessages();
    }
    return $this->messages;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessagesToArray(bool $reload = FALSE): array {
    $messages = $this->getMessages($reload);
    $messages2 = [];
    foreach ($messages as $message) {
      $messages2[] = $message->toArray();
    }
    return $messages2;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessage($message_id): ?MessageInterface {
    if (empty($this->messages)) {
      $this->loadMessages();
    }
    return $this->messages[$message_id] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getBackendConfigurationValue(string $key): mixed {
    return $this->aichat_type->getBackendConfigurationValue($key) ?? $this->defineBackendConfig()[$key]['default'];
  }

  /**
   * {@inheritdoc}
   */
  public function setBackendConfigurationValue(string $key, mixed $value): void {
    $this->aichat_type->getBackendConfigurationValue($key, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function setAIChat(EntityInterface $aichat): void {
    $this->aichat = $aichat;
  }

  /**
   * {@inheritdoc}
   */
  public function setAIChatType(EntityInterface $aichat_type): void {
    $this->aichat_type = $aichat_type;
  }

  /**
   * {@inheritdoc}
   */
  public function handleException(\Exception $e): array {

    // Log exception
    $logger = \Drupal::logger('aichat');  // TODO. add backend name
    Error::logException($logger, $e);

    // Prepare simplified exception array
    $data = Error::decodeException($e);

    unset($data['exception']);
    unset($data['@backtrace_string']);
    unset($data['%file']);
    unset($data['%line']);
    unset($data['backtrace']);

    $data['time'] = time();
    $data['date'] = date('Y-m-d H:i:s', $data['time']);

    return json_decode(json_encode($data), TRUE);
  }
}