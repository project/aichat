<?php

namespace Drupal\aichat\Plugin;

use Drupal\aichat\MessageInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines an interface for AI chat backend plugins.
 */
interface AIChatBackendInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * Overrides the default message object with a custom one.
   *
   * @return MessageInterface
   *   The new message object.
   */
  public function createNewMessageObject(): MessageInterface;

  /**
   * Defines backend configuration default values.
   *
   * @return array
   *   An associative array where the keys are the configuration keys and the
   *   default values.
   */
  public function defineBackendConfig(): array;

  /**
   * Builds the backend configuration form.
   */
  public function buildBackendConfigForm(array $form, FormStateInterface $form_state): array;

  /**
   * Validates the backend configuration form.
   */
  public function validateBackendConfigForm(array $form, FormStateInterface $form_state): void;

  /**
   * Loads the messages from the backend.
   */
  public function loadMessages(): void;

  /**
   * Saves a message to the backend.
   *
   * @param MessageInterface $message
   *   The message object to be saved.
   */
  public function saveMessage(MessageInterface $message): void;

  /**
   * Sends a message to the backend.
   *
   * @param MessageInterface $message
   *   The message object to be sent.
   *
   * @return array
   *   An array of any issues that occurred.
   */
  public function sendMessage(MessageInterface $message): array;

  /**
   * Repeats a previously sent message.
   *
   * @param MessageInterface $response_message
   *   The response message to be repeated.
   *
   * @return array
   *   An array of any issues that occurred.
   */
  public function repeatMessage(MessageInterface $response_message): array;

  /**
   * Returns the plugin ID for this instance.
   *
   * @return string
   *   The plugin ID.
   */
  public function getPluginId(): string;

  /**
   * Returns the plugin label for this instance.
   *
   * @return string
   *   The plugin label.
   */
  public function getPluginLabel(): string;

  /**
   * Returns an array of all loaded messages
   *
   * @param bool $reload
   *   Force reload of messages from backend if set to TRUE
   *
   * @return array
   *   An array of all loaded messages objects.
   */
  public function getMessages(bool $reload = FALSE): array;

  /**
   * Returns an array of all loaded messages in the serialized form (as array).
   *
   * @param bool $reload
   *   Force reload of messages from backend if set to TRUE
   *
   * @return array
   *   An array of all loaded messages as an array
   */
  public function getMessagesToArray(bool $reload): array;

  public function getMessage(string $message_id): ?MessageInterface;

  /**
   * Returns the value of a backend configuration key
   *
   * @param string $key
   *   Backend configuration key
   *
   * @return mixed
   *   Value of the configuration key, or its default value
   */
  public function getBackendConfigurationValue(string $key): mixed;

  /**
   * Updates the value of a backend configuration key
   *
   * @param string $key
   *   Backend configuration key
   * @param mixed $value
   *   New value of the configuration key
   */
  public function setBackendConfigurationValue(string $key, mixed $value): void;

  /**
   * Sets AI chat entity
   *
   * @param \Drupal\Core\Entity\EntityInterface $aichat
   *   AI chat entity object reference
   */
  public function setAIChat(EntityInterface $aichat): void;

  /**
   * Sets AI chat type entity this plugin is extending
   *
   * @param \Drupal\Core\Entity\EntityInterface $aichat_type
   *   AI chat type entity object reference
   */
  public function setAIChatType(EntityInterface $aichat_type): void;

  /**
   * Handles exceptions that are thrown when using the plugin.
   *
   * @param \Exception $e
   *   The thrown exception.
   *
   * @return array
   *   An array including details about the exception.
   */
  public function handleException(\Exception $e): array;

}