<?php

namespace Drupal\aichat;

use Drupal\aichat\Plugin\AIChatBackendInterface;
use Drupal\aichat\MessageInterface;

/**
 * Provides a base class for a Message.
 */
class MessageBase implements MessageInterface {

  // Properties of the message.
  protected string $id;

  protected int $created;

  protected int $modified;

  protected $user_id;

  protected string $role; // For example: user, assistant

  protected array $response_data = [];

  protected array $content = [];

  protected array $errors = [];

  protected array $flags = [];

  // Backend plugin reference
  protected AIChatBackendInterface $backend;

  /**
   * Construct the MessageBase object.
   *
   * @param AIChatBackendInterface $backend
   *   The backend plugin used for managing the message.
   */
  public function __construct(AIChatBackendInterface $backend) {
    $this->backend = $backend;
    $this->initDefaults();
  }

  /**
   * Initializes the default values for class properties.
   */
  private function initDefaults(): void {
    $defaults = [
      'user_id' => NULL,
      'created' => time(),
      'modified' => time()
    ];

    foreach ($defaults as $key => $val) {
      $this->{$key} = $val;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): ?string {
    return $this->id ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setId($id): void {
    $this->id = (string) $id;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserId(): mixed {
    return $this->user_id ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setUserId($user_id): void {
    $this->user_id = $user_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getContent():array {
    return $this->content ?? [];
  }

  /**
   * Set the content message to display to the user.
   *
   * @param array $content
   */
  private function setContent(array $content): void {
    $this->content = $content;
  }

  /**
   * {@inheritdoc}
   */
  public function setText(string $text_value, string $key = '0'): void {
    $this->content[$key]['text'] = $text_value;
  }

  /**
   * {@inheritdoc}
   */
  public function getText(): string {
    $text = [];
    foreach ($this->getContent() as $key => $row) {
      foreach ($row as $type => $val) {
        if ($type != 'text') continue;
        $text[] = $val;
      }
    }
    return implode(PHP_EOL, $text);
  }

  /**
   * {@inheritdoc}
   */
  public function setError(array $data): void {
    $this->errors[] = $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getErrors(): array {
    return $this->errors;
  }

  /**
   * {@inheritdoc}
   */
  public function getRole():string {
    return $this->role;
  }

  /**
   * {@inheritdoc}
   */
  public function setRole(string $role): void {
    $this->role = $role;
  }

  /**
   * {@inheritdoc}
   */
  public function getResponseData():array {
    return $this->response_data;
  }

  /**
   * {@inheritdoc}
   */
  public function setResponseData(array $data): void {
    $this->response_data = $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreated(): ?int {
    return $this->created ?? null;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreated(int $created): void {
    $this->created = $created;
    $this->setModified($created);
  }

  /**
   * {@inheritdoc}
   */
  public function getModified(): ?int {
    return $this->modified ?? null;
  }

  /**
   * {@inheritdoc}
   */
  public function setModified(int $modified): void {
    $this->modified = $modified;
  }

  /**
   * {@inheritdoc}
   */
  public function getFlags(): array {
    return $this->flags;
  }

  /**
   * {@inheritdoc}
   */
  public function setFlag(string $flag): void {
    $this->flags[$flag] = $flag;
  }

  /**
   * {@inheritdoc}
   */
  public function removeFlag(string $flag):bool {
    $exist = $this->hasFlag($flag);

    unset($this->flags[$flag]);

    return $exist;
  }

  /**
   * {@inheritdoc}
   */
  public function hasFlag(string $flag):bool {
    return !empty($this->flags[$flag]);
  }

  /**
   * {@inheritdoc}
   */
  public function isRepeatable():bool {
    return $this->hasFlag('error');
  }

  /**
   * {@inheritdoc}
   */
  public function getBackend(): AIChatBackendInterface {
    return $this->backend;
  }

  /**
   * {@inheritdoc}
   */
  public function save(): void {
    $this->backend->saveMessage($this);
  }

  /**
   * {@inheritdoc}
   */
  public function send(): array {
    return $this->backend->sendMessage($this);
  }

  /**
   * {@inheritdoc}
   */
  public function repeat(): array {
    return $this->backend->repeatMessage($this);
  }

  /**
   * {@inheritdoc}
   */
  public function setException(\Exception $exception): void {
    $prepared_data = $this->backend->handleException($exception);
    $this->setFlag('error');
    $this->setError($prepared_data);
  }

  /**
   * {@inheritdoc}
   */
  public function toArray(): array {
    return [
      'id' => $this->id,
      'user_id' => $this->user_id,
      'created' => $this->created,
      'modified' => $this->modified,
      'role' => $this->role,
      'content' => $this->content,
      'flags' => $this->flags,
      'errors' => $this->errors,
      'response_data' => $this->response_data
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setValuesFromArray(array $values): void {
    if (isset($values['id'])) {
      $this->setId($values['id']);
    }
    if (isset($values['user_id'])) {
      $this->setUserId($values['user_id']);
    }
    if (isset($values['created'])) {
      $this->setCreated($values['created']);
    }
    if (isset($values['modified'])) {
      $this->setModified($values['modified']);
    }
    if (isset($values['role'])) {
      $this->setRole($values['role']);
    }
    if (isset($values['content'])) {
      $this->setContent($values['content']);
    }
    if (isset($values['flags'])) {
      foreach ($values['flags'] as $flag) {
        $this->setFlag($flag);
      }
    }
    if (isset($values['errors'])) {
      foreach ($values['errors'] as $error_data) {
        $this->setError($error_data);
      }
    }
    if (isset($values['response_data'])) {
      $this->response_data = $values['response_data'];
    }
  }

}