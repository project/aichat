<?php

namespace Drupal\aichat\Form;

use Drupal\aichat\AIChatBackendManager;
use Drupal\aichat\Entity\AIChat;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use League\CommonMark\GithubFlavoredMarkdownConverter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for sending a message in a chat.
 */
class ConversationForm extends FormBase {

  const AJAX_WRAPPER = "aichat-conversation-form";

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The aichat_backend plugin manager.
   *
   * @var \Drupal\aichat\AIChatBackendManager
   */
  protected $aichatBackendManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The aichat entity.
   *
   * @var \Drupal\aichat\Entity\AIChat
   */
  protected $entity;

  /**
   * The backend.
   *
   * @var \Drupal\aichat\Plugin\AIChatBackend\AIChatBackendBase
   */
  protected $backend;

  protected $default_user_picture;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AIChatBackendManager $aichat_backend_manager, AccountInterface $current_user,
                              ExtensionPathResolver $extension_path_resolver) {
    $this->entityTypeManager = $entity_type_manager;
    $this->aichatBackendManager = $aichat_backend_manager;
    $this->currentUser = $current_user;
    $this->extensionPathResolver = $extension_path_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.aichat_backend'),
      $container->get('current_user'),
      $container->get('extension.path.resolver')
    );
  }

  public function setEntity(EntityInterface $entity) {

    if (!$entity instanceof AIChat) {
      throw new \InvalidArgumentException('Not AIChat entity provided');
    }

    $this->eid = $entity->id();
    $this->load($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aichat_conversation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $this->load();

    $form['#prefix'] = '<div id="'.self::AJAX_WRAPPER.'">';
    $form['#suffix'] = '</div>';

    $form['#attached']['library'][] = 'aichat/conversationForm';

    $ajax = [
      'wrapper' => self::AJAX_WRAPPER,
      'callback' => '::ajax'
    ];

    $form['status'] = [
      '#type' => 'status_messages',
      '#weight' => -50
    ];

    $form['messages'] = $this->buildMessages();

    $form['new_message_text'] = [
      '#type' => 'textarea',
      '#placeholder' => $this->t('Enter message'),
      '#access' => $this->isChatEnabled()
    ];

    $form['send'] = [
      '#name' => 'send',
      '#type' => 'submit',
      '#value' => $this->t('Send'),
      '#ajax' => $ajax,
      '#access' => $this->isChatEnabled()
    ];

    $form['details'] = [
      '#name' => 'defails',
      '#type' => 'details',
      '#title' => $this->t('Additional settings'),
      '#open' => false,
      '#access' => $this->isAdmin() && $this->isDebugEnabled()
    ];

    $form['details']['get_messages'] = [
      '#name' => 'get_messages',
      '#type' => 'submit',
      '#value' => $this->t('Get messages'),
      '#access' => $this->isAdmin() && $this->isDebugEnabled(),
      '#ajax' => $ajax
    ];

    $this->unload();
    return $form;
  }

  public function buildMessages() {

    $messages = $this->backend->getMessages($reload = TRUE);

    // TODO. replace with Markdown module after it releases Drupal 10 version
    $markdownConverter = new GithubFlavoredMarkdownConverter([
      //'html_input' => 'escape',     // optional line if text formats are configured correctly
      'allow_unsafe_links' => false,
      'max_nesting_level' => 10
    ]);

    $ajax = [
      'wrapper' => self::AJAX_WRAPPER,
      'callback' => '::ajax'
    ];

    $build = [
      '#type' => 'container'
    ];

    $users = $this->loadUsers($messages);

    $i = 1;
    foreach ($messages as $key => $message) {

      $role = $message->getRole();

      if (!in_array($role, ['assistant', 'user'])) continue;

      $role_class = 'aichat-message-role-'.$role;
      $msg_key = "message_$i";

      $build[$msg_key] = [
        '#type' => 'container',
        '#attributes' => ['class' => ['aichat-message', $role_class]]
      ];

      $build[$msg_key]["message_user"] = $this->buildAvatar($message, $users);
      $build[$msg_key]["message_content"] = [
        '#type' => 'container',
        '#attributes' => ['class' => ["aichat-message-content"]]
      ];

      foreach ($message->getFlags() as $key2 => $flag_name) {
        $build[$msg_key]["message_content"]['#attributes']['class'][] = "flag-$flag_name";
      }

      foreach ($message->getContent() as $key2 => $row) {
        foreach ($row as $type => $val) {
          switch ($type) {
            case 'text':
              $build[$msg_key]["message_content"][$key2] = [
                '#type' => 'processed_text',
                '#text' => $markdownConverter->convert($val),
                '#format' => 'aichat_message'
              ];
              break;
          }
        }
      }

      if ($message->hasFlag('error')) {
        $build[$msg_key]["message_content"][$key2] = [
          '#type' => 'processed_text',
          '#text' => '<p>Error has occured</p>',
          '#format' => 'aichat_message'
        ];
      }

      if ($message->isRepeatable()) {
        $button_name = 'repeat_'.$i;
        $build[$msg_key]["message_content"][$button_name] = [
          '#name' => $button_name,
          '#message_id' => $key,
          '#type' => 'submit',
          '#value' => $this->t('Repeat'),
          '#ajax' => $ajax,
          '#access' => $this->isChatEnabled()
        ];
      }

      $i++;
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement()['#name'];

    if ($button == 'send') {

      $new_message = $form_state->getValue('new_message_text');

      if (empty($new_message)) {
        $form_state->setError($form['new_message_text'], $this->t('Please fill message form.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->load();

    $button = $form_state->getTriggeringElement();
    $result = [];

    switch ($button['#name']) {

      case 'send':
        $message_text = $form_state->getValue('new_message_text');

        // create new message object and sent it
        $message = $this->backend->createNewMessageObject();
        $message->setText($message_text);
        $message->setRole('user');
        $message->setUserId($this->currentUser->id());
        $result = $message->send();
        break;

      case 'get_messages':

        // print array of messages which will be sent to API to developer
        $messages = $this->backend->getMessageHistoryArray();
        ksm($messages);
        break;
    }

    if (strpos($button['#name'], 'repeat') !== FALSE) {

      // repeat sending particular message
      $message = $this->backend->getMessage($button['#message_id']);
      if (!empty($message)) {
        $result = $message->repeat();
      }
    }

    $this->showStatusMessage($result);
    $form_state->setRebuild();
  }

  /**
   * Helper function to display message returned from backend.
   */
  public function showStatusMessage($result) {

    if (empty($result)) return;

    $function = 'add'.ucfirst(key($result));

    $message = reset($result);

    $this->messenger()->{$function}($message);
  }

  /**
   * Ajax callback.
   */
  public function ajax(array &$form, FormStateInterface $form_state) {
    $selector = '#'.self::AJAX_WRAPPER;

    $response = new AjaxResponse();

    // Replace wrapper with rebuilt form
    $response->addCommand(new ReplaceCommand($selector, $form));

    // Trigger event on javascript side
    $arguments = []; // maybe will be helpfull later
    $response->addCommand(new InvokeCommand($selector, 'aichatFormAjaxSuccess', $arguments));

    $this->unload();
    return $response;
  }

  /**
   * Removes loaded backend context to avoid serializing it
   */
  private function unload() {
    $this->entity = NULL;
    $this->backend = NULL;
  }

  /**
   * Loads various data for the form
   */
  private function load($entity = NULL) {

    // skip if entity context already exist (data is already loaded)
    if (!empty($this->entity)) {
      return;
    }

    if (empty($entity)) {
      $entity = $this->entityTypeManager->getStorage('aichat')->load($this->eid);
    }

    $this->entity = $entity;
    $aichat_type = $this->entityTypeManager->getStorage('aichat_type')->load($entity->bundle());

    // setup backend
    $backendPluginId = $aichat_type->getBackendPluginId();
    $this->backend = $this->aichatBackendManager->createInstance($backendPluginId);
    $this->backend->setAIChat($this->entity);
    $this->backend->setAIChatType($aichat_type);

    // load default user image
    $field = \Drupal\field\Entity\FieldConfig::loadByName('user', 'user', 'user_picture');
    if (!empty($field)) {
      $default_image = $field->getSetting('default_image');

      if (!empty($default_image['uuid'])) {
        $files = $this->entityTypeManager->getStorage('file')->loadByProperties(['uuid' => $default_image['uuid']]);
        $this->default_user_picture = reset($files)->getFileUri();
      }
    }
  }

  /**
   * Builds user profile picture or default css based picture
   */
  public function buildAvatar($message, &$users) {
    $role = $message->getRole();

    $build = [
      '#type' => 'container',
      '#attributes' => ['class' => ["aichat-message-user"]]
    ];

    // default picture defined by CSS
    $build['avatar'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ["aichat-avatar", "default-avatar-$role"],
        'title' => ucfirst($role)
      ]
    ];

    // if user exist, determine user picture
    $user_id = $message->getUserId();

    if (!empty($user_id) && !empty($users[$user_id])) {

      $user = $users[$user_id];
      $username = $user->getAccountName();

      if ($user->hasField('user_picture')) {

        if (!$user->get('user_picture')->isEmpty()) {
          $image_uri = $user->get('user_picture')->entity->getFileUri();
        }
        elseif (!empty($this->default_user_picture)) {
          $image_uri = $this->default_user_picture;
        }
      }

      if (!empty($image_uri)) {
        $build['avatar'] = [
          '#theme' => 'image_style',
          '#style_name' => 'thumbnail',
          '#uri' => $image_uri,
          '#attributes' => [
            'class' => ["aichat-avatar", "picture-avatar-$role"],
            'title' => $username
          ]
        ];
      }
    }
    return $build;
  }

  /**
   * Helper function to load users.
   */
  public function loadUsers($messages) {
    $user_ids = [];
    foreach ($messages as $message) {
      $userId = $message->getUserId();

      if (!empty($userId)) {
        $user_ids[$userId] = $userId;
      }
    }

    if (empty($user_ids)) return [];

    return $this->entityTypeManager->getStorage('user')->loadMultiple($user_ids);
  }

  public function isChatEnabled() {
    return $this->currentUser->hasPermission("create message on own aichat entity") && $this->entity->isPublished();
  }

  public function isDebugEnabled() {
    return $this->currentUser->hasPermission("access devel information") && function_exists('ksm');
  }

  public function isAdmin() {
    return $this->currentUser->hasPermission("administer aichat");
  }

}