<?php

namespace Drupal\aichat\Form;

use Drupal\aichat\AIChatBackendManager;
use Drupal\aichat\Plugin\AIChatBackendInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * The aichat type entity form.
 *
 * @internal
 */
class AIChatTypeForm extends BundleEntityFormBase {

  /**
   * The AI chat backend plugin manager.
   *
   * @var \Drupal\aichat\AIChatBackendManager
   */
  protected $aiChatBackendPluginManager;

  /**
   * AIChatTypeForm constructor.
   *
   * @param \Drupal\aichat\AIChatBackendManager $ai_chat_backend_plugin_manager
   *   The AI chat backend plugin manager.
   */
  public function __construct(AIChatBackendManager $ai_chat_backend_plugin_manager) {
    $this->aiChatBackendPluginManager = $ai_chat_backend_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.aichat_backend')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $entity = $this->entity;

    if ($this->operation == 'add') {
      $form['#title'] = $this->t('Add AI conversation type');
    }
    else {
      $form['#title'] = $this->t('Edit %label custom type', ['%label' => $entity->label()]);
    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#description' => $this->t("Provide a label for this type to help identify it in the administration pages."),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\aichat\Entity\AIChatType::load',
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#default_value' => $entity->getDescription(),
      '#description' => $this->t('Enter a description for this type.'),
      '#title' => $this->t('Description'),
    ];

    $definitions = $this->aiChatBackendPluginManager->getDefinitions();
    $options = array_map(function($definition) {
      return $definition['label'];
    }, $definitions);

    $description = $this->t('The underlying backend for this conversation type.');
    if (empty($options)) {
      $description .= ' '.$this->t('Please install some of the backend modules. List of available backends is on module\'s project page. Alternatively you can enable "AI Chat backend example" module.');
    }

    $selected_backend = $this->entity->getBackendPluginId() ?? key($options);

    $form['backend'] = [
      '#type' => 'select',
      '#title' => $this->t('Backend'),
      '#description' => $description,
      '#options' => $options,
      '#default_value' => $selected_backend,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::updateBackendConfigurationForm',
        'wrapper' => 'backend-configuration-form',
        'method' => 'replace',
        'effect' => 'fade',
      ],
    ];

    $form['backend_configuration'] = $this->buildBackendConfigForm($selected_backend, $form_state);
    $form['backend_configuration']['#access'] = !empty($options);

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->validateBackendConfigForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $backend_id = $form_state->getValue('backend');
    $backend_plugin = $this->aiChatBackendPluginManager->createInstance($backend_id);
    $config_keys = array_keys($backend_plugin->defineBackendConfig());

    $reserved = ['id', 'label', 'description', 'backend', 'backend_configuration'];
    if (array_intersect($config_keys, $reserved)) {
      throw \Exception ('Backend plugin config cannot use a reserved schema key.');
    }

    foreach ($config_keys as $key) {
      $this->entity->setBackendConfigurationValue($key, $form_state->getValue($key));
    }

    $status = $entity->save();

    $edit_link = $this->entity->toLink($this->t('Edit'), 'edit-form')->toString();
    $logger = $this->logger('aichat');
    if ($status == SAVED_UPDATED) {
      $this->messenger()->addStatus($this->t('Custom type %label has been updated.', ['%label' => $entity->label()]));
      $logger->notice('Custom type %label has been updated.', ['%label' => $entity->label(), 'link' => $edit_link]);
    }
    else {
      $this->messenger()->addStatus($this->t('Custom type %label has been added.', ['%label' => $entity->label()]));
      $logger->notice('Custom type %label has been added.', ['%label' => $entity->label(), 'link' => $edit_link]);
    }

    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
  }

  /**
   * Ajax callback.
   */
  public function updateBackendConfigurationForm(array $form, FormStateInterface $form_state): array {

    $selected_backend = $form_state->getValue('backend');

    $form['backend_configuration'] = $this->buildBackendConfigForm($selected_backend, $form_state);

    return $form['backend_configuration'];
  }

  /**
   * Get backend configuration form defined in backend plugin.
   */
  private function buildBackendConfigForm($backend_id, FormStateInterface $form_state) {

    $element = [
      '#type' => 'fieldset',
      '#attributes' => ['id' => 'backend-configuration-form'],
    ];

    if (empty($backend_id)) return $element;

    $backend_plugin = $this->getBackendPlugin($backend_id);
    $backend_configuration_form = $backend_plugin->buildBackendConfigForm([], $form_state);
    $element = array_merge($element, $backend_configuration_form);

    $label = $backend_plugin->getPluginDefinition()['label'];
    $element['#title'] = $this->t("@label configuration form", ['@label' => $label]);

    return $element;
  }

  /**
   * Validate backend configuration form defined in backend plugin.
   */
  private function validateBackendConfigForm(array $form, FormStateInterface $form_state) {

    $backend_id = $form_state->getValue('backend');
    $backend_plugin = $this->getBackendPlugin($backend_id);
    $backend_plugin->validateBackendConfigForm($form['backend_configuration'], $form_state);
  }

  /**
   * Helper method to get backend plugin.
   */
  private function getBackendPlugin(string $backend_id): AIChatBackendInterface {
    $backend_plugin = $this->aiChatBackendPluginManager->createInstance($backend_id);
    $backend_plugin->setAIChatType($this->entity);
    return $backend_plugin;
  }

}
