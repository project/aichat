<?php

namespace Drupal\aichat;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a custom entity bundle.
 */
interface AIChatTypeInterface extends ConfigEntityInterface {

  /**
   * Returns the description of the bundle.
   *
   * @return string
   *   The description of the bundle.
   */
  public function getDescription(): ?string;

  /**
   * Returns the selected backend plugin ID for AIChat type.
   *
   * @return string|null
   *   The selected backend plugin name.
   */
  public function getBackendPluginId(): ?string;

  /**
   * Returns configurations for backend for AIChat type.
   *
   * @return array
   *   The configurations for selected backend.
   */
  public function getBackendConfiguration(): array;

  /**
   * Get the value of specific backend configuration.
   * 
   * @param string $key
   *   The key of the backend configuration.
   * 
   * @return mixed
   *   The value of the key for selected backend configuration.
   */
  public function getBackendConfigurationValue(string $key): mixed;

  /**
   * Set the value of specific backend configuration.
   * 
   * @param string $key
   *   The key of the backend configuration.
   * @param mixed $value
   *   The value of the key for selected backend configuration.
   */
  public function setBackendConfigurationValue(string $key, mixed $value): void;

}