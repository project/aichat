<?php

namespace Drupal\aichat\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a AI chat backend item annotation object.
 *
 * @Annotation
 */
class AIChatBackend extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   * 
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   * 
   * @ingroup plugin_translatable
   */
  public $description;
}