<?php

namespace Drupal\aichat;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\aichat\Plugin\AIChatBackendInterface;
use Drupal\aichat\Annotation\AIChatBackend;

/**
 * AIChatBackend plugin manager.
 */
class AIChatBackendManager extends DefaultPluginManager {

  /**
   * Constructs an AIChatBackendManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/AIChatBackend',
      $namespaces,
      $module_handler,
      AIChatBackendInterface::class,
      AIChatBackend::class
    );

    $this->alterInfo('aichat_backend_plugin_info');
    $this->setCacheBackend($cache_backend, 'aichat_backend_plugins');
  }

}