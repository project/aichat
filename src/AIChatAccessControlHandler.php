<?php

namespace Drupal\aichat;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the aichat entity.
 */
class AIChatAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   *
   * Link the activities to the permissions. checkAccess() is called with the
   * $operation as defined in the routing.yml file.
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    // defined in entity type anotation
    $admin_permission = $this->entityType->getAdminPermission();

    // administrators can do everything
    if ($account->hasPermission($admin_permission)) {
      return AccessResult::allowed();
    }

    // only authors can see entities
    if ($account->id() !== $entity->getOwnerId()) {
      return AccessResult::forbidden("User is not author of entity");
    }

    switch ($operation) {

      case 'view':
        $permissions = ['view own aichat entity'];
        return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');

      case 'update':
        $permissions = ['edit own aichat entity'];
        return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');

      case 'delete':
        $permissions = ['delete own aichat entity'];
        return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');
    }
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {

    $admin_permission = $this->entityType->getAdminPermission();
 
    if ($account->hasPermission($admin_permission)) {
      return AccessResult::allowed();
    }
    return AccessResult::allowedIfHasPermission($account, 'create aichat entity');
  }

}
