<?php

namespace Drupal\aichat\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\aichat\AIChatTypeInterface;

/**
 * Defines the aichat type entity.
 *
 * @ConfigEntityType(
 *   id = "aichat_type",
 *   label = @Translation("AI conversation type"),
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\aichat\Form\AIChatTypeForm",
 *       "add" = "Drupal\aichat\Form\AIChatTypeForm",
 *       "edit" = "Drupal\aichat\Form\AIChatTypeForm",
 *       "delete" = "Drupal\aichat\Form\AIChatTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *       "permissions" = "Drupal\user\Entity\EntityPermissionsRouteProviderWithCheck",
 *     },
 *     "list_builder" = "Drupal\aichat\AIChatTypeListBuilder"
 *   },
 *   admin_permission = "administer aichat",
 *   config_prefix = "aichat_type",
 *   bundle_of = "aichat",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "delete-form" = "/admin/structure/aichats/manage/{aichat_type}/delete",
 *     "edit-form" = "/admin/structure/aichats/manage/{aichat_type}",
 *     "collection" = "/admin/structure/aichats/types",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "backend",
 *     "backend_configuration"
 *   }
 * )
 */
class AIChatType extends ConfigEntityBundleBase implements AichatTypeInterface {
  
  /**
   * The ID of the AIChat type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the AIChat type.
   *
   * @var string
   */
  protected $label;

  /**
   * The description of the aichat type.
   *
   * @var string
   */
  protected $description;

  /**
   * The selected backend plugin for this AIChat type.
   *
   * @var string
   */
  protected $backend;

  /**
   * The selected backend plugin specific configuration.
   *
   * @var array
   */
  protected $backend_configuration = [];

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getBackendPluginId(): ?string {
    return $this->backend ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getBackendConfiguration(): array {
    return $this->backend_configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function getBackendConfigurationValue(string $key): mixed {
    return $this->backend_configuration[$key] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setBackendConfigurationValue(string $key, mixed $value): void {
    $this->backend_configuration[$key] = $value;
  }

}