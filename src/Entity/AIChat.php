<?php

namespace Drupal\aichat\Entity;

use Drupal\aichat\AIChatInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * AI chat conversation entity.
 *
 * @ingroup aichat
 *
 * @ContentEntityType(
 *   id = "aichat",
 *   label = @Translation("Conversation"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\aichat\AIChatListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "storage_schema" = "Drupal\aichat\AIChatStorageSchema", 
 *     "access" = "Drupal\aichat\AIChatAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\aichat\Form\AIChatEntityForm",
 *       "add" = "Drupal\aichat\Form\AIChatEntityForm",
 *       "edit" = "Drupal\aichat\Form\AIChatEntityForm",
 *       "delete" = "Drupal\aichat\Form\AIChatDeleteForm",
 *     },
 *   },
 *   base_table = "aichat",
 *   admin_permission = "administer aichat",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *     "published" = "active",
 *   },
 *   links = {
 *     "canonical" = "/aichat/{aichat}",
 *     "add-page" = "/aichat/add",
 *     "add-form" = "/aichat/add/{type}",
 *     "edit-form" = "/aichat/{aichat}/edit",
 *     "delete-form" = "/aichat/{aichat}/delete",
 *     "collection" = "/admin/content/aichat"
 *   },
 *   bundle_entity_type = "aichat_type",
 *   field_ui_base_route = "entity.aichat_type.edit_form",
 * )
 */
class AIChat extends ContentEntityBase implements EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface, AIChatInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;
  use EntityOwnerTrait;

  protected $data_cache;

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);
    if(empty($entity_type)){
      return $fields;
    }

    $fields += static::publishedBaseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $owner_key = $entity_type->getKey('owner');
    $fields[$owner_key]
      ->setLabel(t('Owner'))
      ->setDescription(t('The username of who started this conversation.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $published_key = $entity_type->getKey('published');
    $fields[$published_key]
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 120,
      ])
      ->setLabel(t('Enabled'))
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the conversation was created.'))
      ->setDisplayOptions('form', [
        'region' => 'hidden'
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the conversation was last updated.'));

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription('')
      ->setDefaultValue('')
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['data'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Data'))
      ->setDescription('')
      ->setRequired(FALSE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'region' => 'hidden'
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function postLoad(EntityStorageInterface $storage, array &$entities) {
    parent::postLoad($storage, $entities);

    foreach ($entities as $entity) {
      $entity->data_cache = json_decode($entity->data->value ?? '', TRUE);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDataNestedValue(mixed $key): mixed {
    if (empty($this->data_cache)) {
      return NULL;
    }
    return NestedArray::getValue($this->data_cache, (array) $key);
  }

  /**
   * {@inheritdoc}
   */
  public function setDataNestedValue(mixed $key, mixed $value, bool $save = TRUE): void {
    if (empty($this->data_cache)) {
      $this->data_cache = [];
    }
    NestedArray::setValue($this->data_cache, (array) $key, $value, TRUE);

    if ($value === NULL) {
      NestedArray::unsetValue($this->data_cache, $key);
    }

    if ($save) {
      $this->saveData();
    }
  }

  /**
   * @deprecated, use new getDataNestedValue()
   */
  public function getDataValue(string $key): mixed {
    return $this->getDataNestedValue($key);
  }

  /**
   * @deprecated, use new getDataNestedValue()
   */
  public function getDataSubValue(string $key, string $subkey): mixed {
    return $this->getDataNestedValue([$key, $subkey]);
  }

  /**
   * @deprecated, use new setDataNestedValue()
   */
  public function setDataValue(string $key, $val, $save = TRUE): void {
    $this->setDataNestedValue($key, $val, $save);
  }

  /**
   * @deprecated, use new setDataNestedValue()
   */
  public function setDataSubValue(string $key, string $subkey, $val, bool $save = TRUE): void {
    $this->setDataNestedValue([$key, $subkey], $val, $save);
  }

  /**
   * {@inheritdoc}
   */
  public function saveData(): void {
    $this->set('data', json_encode($this->data_cache ?? [], JSON_PRETTY_PRINT));
  }

}
