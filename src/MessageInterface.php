<?php

namespace Drupal\aichat;

use Drupal\aichat\Plugin\AIChatBackendInterface;

/**
 * Defines an interface for a Message.
 */
interface MessageInterface {

  /**
   * Gets the ID of the message.
   *
   * @return string|null
   *   The ID of the message, or NULL if not set.
   */
  public function getId(): ?string;

  /**
   * Sets the ID of the message.
   *
   * @param string $id
   *   The ID of the message.
   */
  public function setId($id): void;

  /**
   * Gets the user ID associated with the message.
   *
   * @return mixed
   *   The user ID associated with the message.
   */
  public function getUserId(): mixed;

  /**
   * Sets the user ID associated with the message.
   *
   * @param mixed $user_id
   *   The user ID associated with the message.
   */
  public function setUserId($user_id): void;

  /**
   * Gets the content of the message.
   *
   * @return array
   *   The content of the message.
   */
  public function getContent(): array;

  /**
   * Set text content.
   */
  public function setText(string $text_value, string $key = '0');

  /**
   * Gets the text content of the message.
   *
   * @return string
   *   The text content of the message.
   */
  public function getText(): string;

  public function setError(array $data): void;

  public function getErrors(): array;

  /**
   * Gets the role of the message.
   *
   * @return string
   *   The role of the message.
   */
  public function getRole(): string;

  /**
   * Sets the role of the message.
   *
   * @param string $role
   *   The role of the message.
   */
  public function setRole(string $role): void;

  public function getResponseData(): array;

  public function setResponseData(array $data): void;

  /**
   * Gets the creation timestamp of the message.
   *
   * @return int|null
   *   The creation timestamp of the message, or NULL if not set.
   */
  public function getCreated(): ?int;

  /**
   * Sets the creation timestamp of the message.
   *
   * @param int $created
   *   The creation timestamp of the message.
   */
  public function setCreated(int $created): void;

  /**
   * Gets the modified timestamp of the message.
   *
   * @return int|null
   *   The modified timestamp of the message, or NULL if not set.
   */
  public function getModified(): ?int;

  /**
   * Sets the modified timestamp of the message.
   *
   * @param int $modified
   *   The modified timestamp of the message.
   */
  public function setModified(int $modified): void;

  /**
   * Gets the flags associated with the message.
   *
   * @return array
   *   The flags associated with the message.
   */
  public function getFlags(): array;

  /**
   * Sets a flag for the message.
   *
   * @param string $flag
   *   The flag to set.
   */
  public function setFlag(string $flag): void;

  /**
   * Removes a flag from the message.
   *
   * @param string $flag
   *   The flag to remove.
   *
   * @return bool
   *   TRUE if the flag was removed, FALSE otherwise.
   */
  public function removeFlag(string $flag): bool;

  /**
   * Checks if the message has a certain flag.
   *
   * @param string $flag
   *   The flag to check.
   *
   * @return bool
   *   TRUE if the message has the flag, FALSE otherwise.
   */
  public function hasFlag(string $flag): bool;

  /**
   * Checks if the message is repeatable.
   *
   * @return bool
   *   TRUE if the message is repeatable, FALSE otherwise.
   */
  public function isRepeatable(): bool;

  /**
   * Gets the backend object reference.
   *
   * @return AIChatBackendInterface
   *   The backend object reference.
   */
  public function getBackend(): AIChatBackendInterface;

  /**
   * Save exception data to the message.
   *
   * @param \Exception $exception
   *     The caught exception
   */
  public function setException(\Exception $exception): void;

  /**
   * Saves the message.
   */
  public function save(): void;

  /**
   * Sends the message and returns the response data.
   *
   * @return array
   *   The response data.
   */
  public function send(): array;

  /**
   * Repeats the message by resending it and returns the response data.
   *
   * @return array
   *   The response data.
   */
  public function repeat(): array;

  /**
   * Converts the message to an array.
   *
   * @return array
   *   The message as an array.
   */
  public function toArray(): array;

  /**
   * Sets the values of the message properties from an array.
   *
   * @param array $values
   *   An associative array where keys are property names and
   *   values are their respective values.
   */
  public function setValuesFromArray(array $values): void;
}