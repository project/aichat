CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation and usage
 * For developers

INTRODUCTION
------------

Flexible multimedia chat interface, pluggable to many backends and for many usecases.

For more information visit project page https://drupal.org/project/aichat

REQUIREMENTS
------------

 * Depends on [Key module](https://www.drupal.org/project/key) for APi key management.

 * Depends on `league/commonmark` library for markdown integration. Maybe will be replaced by Markdown module at some point.

 * Depends on `openai-php/client` library for connection with OpenAI API.

RECOMMENDED MODULES
-------------------

 * [AI prompt engineering](https://www.drupal.org/project/aiprompt) enables to talk with AI about Drupal content.

INSTALLATION AND USAGE
----------------------

- Follow the standard Drupal installation instructions:
  https://www.drupal.org/docs/extending-drupal/installing-modules

- Create and configure conversation type under `/admin/structure/aichat`

- Create new conversation `/admin/content/aichat` and have a chat with AI.

FOR DEVELOPERS
--------------
- `aichat_backend_example` sub-module contains various examples how to easily create custom backends.
