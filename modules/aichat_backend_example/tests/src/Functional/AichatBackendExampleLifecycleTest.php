<?php

namespace Drupal\Tests\aichat_backend_example\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the installation and uninstallation processes of the aichat_backend_example submodule.
 */
class AichatBackendExampleLifecycleTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['aichat', 'aichat_backend_example'];

  /**
   * Tests aichat_backend_example module installation.
   */
  public function testModuleInstallation() {
    $this->assertTrue(\Drupal::moduleHandler()->moduleExists('aichat_backend_example'), 'The aichat_backend_example module is installed.');
  }

  /**
   * Tests aichat_backend_example module uninstallation.
   */
  public function testModuleUninstallation() {
    \Drupal::service('module_installer')->uninstall(['aichat_backend_example']);
    $this->assertFalse(\Drupal::moduleHandler()->moduleExists('aichat_backend_example'), 'The aichat_backend_example module is uninstalled.');
  }
  
}
