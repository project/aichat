<?php

namespace Drupal\aichat_backend_example;

use Drupal\aichat\MessageBase;

class Message extends MessageBase {

  /**
   * {@inheritdoc}
   */
  public function setResponseData(array $data):void {
    parent::setResponseData($data);

    if (empty($this->getId())) {
      $this->setId( $data['id'] );
    }

    $this->setCreated( $data['created'] );
    $this->setText( $data['choices'][0]['message']['content'] );
    $this->setRole( $data['choices'][0]['message']['role'] );
    $this->removeFlag('error');
  }
}