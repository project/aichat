<?php

namespace Drupal\aichat_backend_example\Plugin\AIChatBackend;

use Drupal\aichat\MessageInterface;
use Drupal\aichat\Plugin\AIChatBackend\DefaultBackend;
use Drupal\aichat_backend_example\Message;

/**
 * Example backend for AI Chat no. 1
 *
 * Demonstrates an easiest way to create a fully functional backend by extending
 * DefaultBackend and changing some methods.
 *
 * This method is good if modifications don't differ too much from default
 * backend. But if you want to implement completely different backend, then look
 * at another example in ExampleBackend2.php file.
 *
 * @AIChatBackend(
 *   id = "example_backend",
 *   label = @Translation("AI chat example backend 1"),
 *   description = @Translation("Example backend for AI Chat extending DefaultBackend.")
 * )
 */
class ExampleBackend extends DefaultBackend {

  /**
   * Demonstrates, that overriding this method allows to swap message class
   * with a custom one.
   */
  public function createNewMessageObject(): MessageInterface {
    return new Message($this);
  }

}
